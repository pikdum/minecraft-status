FROM python:3-alpine
MAINTAINER pikdum <pikdum@kuudere.moe>
WORKDIR /app/
RUN apk add --no-cache curl
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE 8080
HEALTHCHECK CMD curl --fail http://localhost:8080/health || exit 1
CMD python minecraft-status.py
