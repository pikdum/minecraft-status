import json, os
from flask import Flask, request, Response
from flask_cors import CORS, cross_origin
from waitress import serve
from minestat import MineStat

app = Flask(__name__)
CORS(app)

def clean(string):
    if not isinstance(string, str):
        return string
    return string.replace('\u0000', '')

@cross_origin
@app.route("/status")
def status():
    s = MineStat(os.getenv('MINECRAFT_HOST'),
                 int(os.getenv('MINECRAFT_PORT')))
    d = {
        "online": s.online,
        "address": s.address,
        "port": s.port,
        "version": s.version,
        "motd": s.motd,
        "current_players": s.current_players,
        "max_players": s.max_players
    }
    d = {k: clean(v) for k, v in d.items()}
    r = Response(json.dumps(d, sort_keys=True))
    r.headers['Content-Type'] = 'application/json'
    return r, 200

@app.route("/health")
def health():
    d = {
        "status": "success"
    }
    r = Response(json.dumps(d, sort_keys=True))
    r.headers['Content-Type'] = 'application/json'
    return r, 200

if __name__ == "__main__":
    serve(app)
